# GraphQL-MongoDB-Example

All the important code is in `src/start.js`.

Install, build and run:

```
yarn install
yarn run build
yarn start
```

For Local Development 

```
npm run startdev
```

queryExamples
```
mutation {
  createPost(title:"hello", content:"world") {
    _id
    title
    content
  }
}

query {
  posts {
    _id
    title
    content
  }
}


mutation {
  createComment(postId: "584ebf8bee8d98127efb080c", content: "I like the way you say hello world.") {
    _id
    postId
    content
  }
}

query {
  posts {
    _id
    title
    content
    comments {
      _id
      postId
      content
    }
  }
}
```